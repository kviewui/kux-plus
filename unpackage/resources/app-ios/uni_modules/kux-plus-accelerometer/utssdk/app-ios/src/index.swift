import DCloudUTSFoundation;
import DCloudUniappRuntime;
public var UniErrorSubject = "uts-api";
public var MyAPIErrors: Map<MyApiErrorCode, String> = Map([
    [
        9010001,
        "custom error mseeage1"
    ],
    [
        9010002,
        "custom error mseeage2"
    ]
]);
@objc(UTSSDKModulesKuxPlusAccelerometerMyApiFailImpl)
@objcMembers
public class MyApiFailImpl : UniError, MyApiFail {
    public init(_ errCode: MyApiErrorCode){
        super.init();
        self.errSubject = UniErrorSubject;
        self.errCode = errCode;
        self.errMsg = MyAPIErrors.get(errCode) ?? "";
    }
}
public var myApi: MyApi = {
(_ options: MyApiOptions) -> Void in
if (options.paramA == true) {
    var res = MyApiResult(UTSJSONObject([
        "fieldA": 85 as NSNumber,
        "fieldB": true,
        "fieldC": "some message"
    ]));
    options.success?(res);
    options.complete?(res);
} else {
    var failResult = MyApiFailImpl(9010001);
    options.fail?(failResult);
    options.complete?(failResult);
}
};
public var myApiSync: MyApiSync = {
(_ paramA: Bool) -> MyApiResult in
var res = MyApiResult(UTSJSONObject([
    "fieldA": 85 as NSNumber,
    "fieldB": paramA,
    "fieldC": "some message"
]));
return res;
};
public func myApiByJs(_ options: MyApiOptions) -> Void {
    return myApi(options);
}
public func myApiSyncByJs(_ paramA: Bool) -> MyApiResult {
    return myApiSync(paramA);
}
@objc(UTSSDKModulesKuxPlusAccelerometerIndexSwift)
@objcMembers
public class UTSSDKModulesKuxPlusAccelerometerIndexSwift : NSObject {
    public static func s_myApiByJs(_ options: MyApiOptions) -> Void {
        return myApiByJs(options);
    }
    public static func s_myApiSyncByJs(_ paramA: Bool) -> MyApiResult {
        return myApiSyncByJs(paramA);
    }
}
