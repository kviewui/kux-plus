(function (vue) {
	'use strict';

	const { registerUTSInterface, initUTSProxyClass, initUTSProxyFunction, initUTSPackageName, initUTSIndexClassName, initUTSClassName } = uni;
	const name = 'kuxPlusAccelerometer';
	const moduleName = 'kux-plus-accelerometer';
	const moduleType = '';
	const errMsg = ``;
	const is_uni_modules = true;
	const pkg = /*#__PURE__*/ initUTSPackageName(name, is_uni_modules);
	const cls = /*#__PURE__*/ initUTSIndexClassName(name, is_uni_modules);
	registerUTSInterface('IAccelerometerOptions',Object.assign({ moduleName, moduleType, errMsg, package: pkg, class: initUTSClassName(name, 'IAccelerometerByJsProxy', is_uni_modules) }, {"methods":{"getCurrentAccelerationByJs":{"async":false,"params":[{"name":"successCB","type":"UTSCallback"},{"name":"errorCB","type":"UTSCallback","default":"UTSNull"}],"return":{}},"watchAccelerationByJs":{"async":false,"params":[{"name":"successCB","type":"UTSCallback"},{"name":"errorCB","type":"UTSCallback","default":"UTSNull"},{"name":"options","type":"UTSSDKModulesKuxPlusAccelerometerAccelerometerOptionJSONObject","default":"UTSNull"}],"return":{}},"clearWatchByJs":{"async":false,"params":[{"name":"watchId","type":"number"}],"return":{}}},"props":[],"setters":{}} ));
	const useAccelerometer = /*#__PURE__*/ initUTSProxyFunction(false, { moduleName, moduleType, errMsg, main: true, package: pkg, class: cls, name: 'useAccelerometerByJs', params: [], return: {"type":"interface","options":"IAccelerometerOptions"}});

	/**
	 * interface.uts
	 * uts插件接口定义文件，按规范定义接口文件可以在HBuilderX中更好的做到语法提示
	 */
	class Acceleration extends UTS.UTSType {
	    static get$UTSMetadata$() {
	        return {
	            kind: 2,
	            get fields() {
	                return {
	                    xAxis: { type: Number, optional: false },
	                    yAxis: { type: Number, optional: false },
	                    zAxis: { type: Number, optional: false }
	                };
	            }
	        };
	    }
	    constructor(options, metadata = Acceleration.get$UTSMetadata$(), isJSONParse = false) {
	        super();
	        this.__props__ = UTS.UTSType.initProps(options, metadata, isJSONParse);
	        this.xAxis = this.__props__.xAxis;
	        this.yAxis = this.__props__.yAxis;
	        this.zAxis = this.__props__.zAxis;
	        delete this.__props__;
	    }
	}
	/**
	 * 监听设备加速度感应器参数
	 */
	class AccelerometerOption extends UTS.UTSType {
	    static get$UTSMetadata$() {
	        return {
	            kind: 2,
	            get fields() {
	                return {
	                    frequency: { type: Number, optional: false }
	                };
	            }
	        };
	    }
	    constructor(options, metadata = AccelerometerOption.get$UTSMetadata$(), isJSONParse = false) {
	        super();
	        this.__props__ = UTS.UTSType.initProps(options, metadata, isJSONParse);
	        this.frequency = this.__props__.frequency;
	        delete this.__props__;
	    }
	}

	class IPlus extends UTS.UTSType {
	    static get$UTSMetadata$() {
	        return {
	            kind: 2,
	            get fields() {
	                return {
	                    accelerometer: { type: "Unknown", optional: false }
	                };
	            }
	        };
	    }
	    constructor(options, metadata = IPlus.get$UTSMetadata$(), isJSONParse = false) {
	        super();
	        this.__props__ = UTS.UTSType.initProps(options, metadata, isJSONParse);
	        this.accelerometer = this.__props__.accelerometer;
	        delete this.__props__;
	    }
	}
	const _sfc_main$1 = /*#__PURE__*/ vue.defineComponent({
	    __name: 'index',
	    setup(__props) {
	        const title = vue.ref('Hello');
	        const plus = new IPlus({
	            accelerometer: useAccelerometer()
	        });
	        const getCurrentAcceleration = () => {
	            plus.accelerometer.getCurrentAcceleration((a) => {
	                uni.__log__('log', 'at pages/index/index.uvue:26', "Acceleration\nx:" + a.xAxis + "\ny:" + a.yAxis + "\nz:" + a.zAxis);
	            });
	        };
	        getCurrentAcceleration();
	        const watchId = plus.accelerometer.watchAcceleration((a) => {
	            uni.__log__('log', 'at pages/index/index.uvue:33', "Acceleration\nx:" + a.xAxis + "\ny:" + a.yAxis + "\nz:" + a.zAxis);
	        }, (err) => {
	            uni.__log__('log', 'at pages/index/index.uvue:35', err);
	        }, { frequency: 2000 });
	        uni.__log__('log', 'at pages/index/index.uvue:38', '10秒后自动关闭监听');
	        // 测试10秒后关闭监听
	        setTimeout(() => {
	            plus.accelerometer.clearWatch(watchId);
	        }, 10000);
	        return (_ctx, _cache) => {
	            return (vue.openBlock(), vue.createElementBlock("view", { class: "content" }, [
	                vue.createElementVNode("image", {
	                    class: "logo",
	                    src: "/static/logo.png"
	                }),
	                vue.createElementVNode("view", { class: "text-area" }, [
	                    vue.createElementVNode("text", { class: "title" }, vue.toDisplayString(vue.unref(title)), 1)
	                ])
	            ]));
	        };
	    }
	});

	const _style_0$1 = {"content":{"":{"display":"flex","alignItems":"center","justifyContent":"center"}},"logo":{"":{"height":"200rpx","width":"200rpx","marginTop":"200rpx","marginBottom":"50rpx"}},"title":{"":{"fontSize":"36rpx","color":"#8f8f94"}}};

	const _export_sfc = (sfc, props) => {
	  const target = sfc.__vccOpts || sfc;
	  for (const [key, val] of props) {
	    target[key] = val;
	  }
	  return target;
	};

	const PagesIndexIndex = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["styles", [_style_0$1]]]);

	__definePage('pages/index/index',PagesIndexIndex);

	const _sfc_main = vue.defineComponent({
	    onLaunch: function () {
	        uni.__log__('log', 'at App.uvue:5', 'App Launch');
	    },
	    onShow: function () {
	        uni.__log__('log', 'at App.uvue:8', 'App Show');
	    },
	    onHide: function () {
	        uni.__log__('log', 'at App.uvue:11', 'App Hide');
	    },
	    onExit: function () {
	        uni.__log__('log', 'at App.uvue:32', 'App Exit');
	    },
	});

	const _style_0 = {"uni-row":{"":{"flexDirection":"row"}},"uni-column":{"":{"flexDirection":"column"}}};

	const App = /* @__PURE__ */ _export_sfc(_sfc_main, [["styles", [_style_0]]]);

	const __global__ = typeof globalThis === 'undefined' ? Function('return this')() : globalThis;
	__global__.__uniX = true;
	function createApp() {
	    const app = vue.createSSRApp(App);
	    return {
	        app
	    };
	}
	createApp().app.mount("#app");

})(Vue);
