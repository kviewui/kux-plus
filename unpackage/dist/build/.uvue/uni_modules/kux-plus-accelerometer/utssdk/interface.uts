/**
 * interface.uts
 * uts插件接口定义文件，按规范定义接口文件可以在HBuilderX中更好的做到语法提示
 */

export type Acceleration  = {
	/**
	 * x轴方向的加速度
	 * @description 获取当前设备x轴方向的加速度，浮点型数据，与物理学中的加速度值一致。
	 */
	xAxis: number
	/**
	 * y轴方向的加速度
	 * @description 获取当前设备y轴方向的加速度，浮点型数据，与物理学中的加速度值一致。
	 */
	yAxis: number
	/**
	 * z轴方向的加速度
	 * @description 获取当前设备z轴方向的加速度，浮点型数据，与物理学中的加速度值一致。
	 */
	zAxis: number
}

/**
 * 获取设备加速度信息成功的回调函数
 */
export type AccelerationSuccessCallback = (acceleration: Acceleration) => void

/**
 * 获取设备加速度信息失败的回调函数
 */
export type AccelerationErrorCallback = (error: Error) => void

/**
 * 监听设备加速度感应器参数
 */
export type AccelerometerOption = {
	/**
	 * 更新加速度信息间隔时间
	 * @description 监听器获取加速度信息的时间间隔，单位为ms，默认值为500ms
	 */
	frequency: number
}

/**
 * 加速度传感器接口
 */
export interface IAccelerometer {
	/**
	 * 获取当前设备的加速度信息
	 * @description 加速度是设备在当前方向上所做相对运动变化（增、减量）的运动传感器。加速度信息包括x、y、z三个方向的信息。 加速度信息可通过successCB回调函数返回。加速度信息获取失败则调用回调函数errorCB。
	 */
	getCurrentAcceleration (successCB: AccelerationSuccessCallback): void
	
	getCurrentAcceleration (successCB: AccelerationSuccessCallback, errorCB: AccelerationErrorCallback | null): void
	
	/**
	 * 监听设备加速度变化信息
	 * @description 加速度是设备在当前方向上所做相对运动变化（增、减量）的运动传感器。加速度信息包括x、y、z三个方向的信息。 watchAcceleration每隔固定时间就获取一次设备的加速度信息，通过successCB回调函数返回。可通过option的frequency参数设定获取设备加速度信息的时间间隔。 加速度信息获取失败则调用回调函数errorCB。
	 */
	watchAcceleration (successCB: AccelerationSuccessCallback): number
	
	watchAcceleration (successCB: AccelerationSuccessCallback, errorCB: AccelerationErrorCallback | null): number
	
	watchAcceleration (successCB: AccelerationSuccessCallback, errorCB: AccelerationErrorCallback | null, options: AccelerometerOption | null): number
	
	/**
	 * 关闭监听设备加速度信息
	 * @description 关闭监听设备加速度信息，应用关闭调用watchAcceleration方法的开启的监听操作。
	 */
	clearWatch (watchId: number): void
}

export declare function useAccelerometer (): IAccelerometer