'use strict';

require('vue');

const { registerUTSInterface, initUTSProxyClass, initUTSProxyFunction, initUTSPackageName, initUTSIndexClassName, initUTSClassName } = uni;
const name = 'kuxPlusAccelerometer';
const moduleName = 'kux-plus-accelerometer';
const moduleType = '';
const errMsg = ``;
const is_uni_modules = true;
const pkg = /*#__PURE__*/ initUTSPackageName(name, is_uni_modules);
registerUTSInterface('IAccelerometerOptions',Object.assign({ moduleName, moduleType, errMsg, package: pkg, class: initUTSClassName(name, 'IAccelerometerByJsProxy', is_uni_modules) }, {"methods":{"getCurrentAccelerationByJs":{"async":false,"params":[{"name":"successCB","type":"UTSCallback"},{"name":"errorCB","type":"UTSCallback","default":"UTSNull"}],"return":{}},"watchAccelerationByJs":{"async":false,"params":[{"name":"successCB","type":"UTSCallback"},{"name":"errorCB","type":"UTSCallback","default":"UTSNull"},{"name":"options","type":"UTSSDKModulesKuxPlusAccelerometerAccelerometerOptionJSONObject","default":"UTSNull"}],"return":{}},"clearWatchByJs":{"async":false,"params":[{"name":"watchId","type":"number"}],"return":{}}},"props":[],"setters":{}} ));
