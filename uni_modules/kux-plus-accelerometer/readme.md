# kux-plus-accelerometer
管理设备加速度传感器，用于获取设备加速度信息，包括x（屏幕水平方向）、y（垂直屏幕水平方向）、z（垂直屏幕平面方向）三个方向的加速度信息。

> **提示**
> 
> 该插件需要打包自定义基座方可使用！

### 方法
+ [getCurrentAcceleration](#api_getCurrentAcceleration): 获取当前设备的加速度信息
+ [watchAcceleration](#api_watchAcceleration): 监听设备加速度变化信息
+ [clearWatch](#api_clearWatch): 关闭监听设备加速度信息

### 对象
+ [Acceleration](#type_Acceleration): 设备加速度信息对象
+ [AccelerometerOption](#type_AccelerometerOption): 监听设备加速度感应器参数

### 回调方法
+ [AccelerometerSuccessCallback](#type_AccelerometerSuccessCallback): 获取设备加速度信息成功的回调函数
+ [AccelerometerErrorCallback](#type_AccelerometerErrorCallback): 获取设备加速度信息失败的回调函数

<a id="api_getCurrentAcceleration"></a>
## getCurrentAcceleration
获取当前设备的加速度信息

```
getCurrentAcceleration (successCB: AccelerationSuccessCallback): void
	
getCurrentAcceleration (successCB: AccelerationSuccessCallback, errorCB: AccelerationErrorCallback | null): void
```

### 说明：
加速度是设备在当前方向上所做相对运动变化（增、减量）的运动传感器。加速度信息包括x、y、z三个方向的信息。 加速度信息可通过successCB回调函数返回。加速度信息获取失败则调用回调函数errorCB。

### 参数：
+ successCB: [AccelerometerSuccessCallback](#type_AccelerometerSuccessCallback) `必选` 获取设备加速度信息成功回调函数
+ errorCB: [AccelerometerErrorCallback](#type_AccelerometerErrorCallback) `可选` 获取设备加速度信息失败回调函数

### 返回值：
`void`

<a id="api_watchAcceleration"></a>
## watchAcceleration
监听设备加速度变化信息

```
watchAcceleration (successCB: AccelerationSuccessCallback): number
	
watchAcceleration (successCB: AccelerationSuccessCallback, errorCB: AccelerationErrorCallback | null): number
	
watchAcceleration (successCB: AccelerationSuccessCallback, errorCB: AccelerationErrorCallback | null, options: AccelerometerOption | null): number
```

### 说明：
加速度是设备在当前方向上所做相对运动变化（增、减量）的运动传感器。加速度信息包括x、y、z三个方向的信息。 watchAcceleration每隔固定时间就获取一次设备的加速度信息，通过successCB回调函数返回。可通过option的frequency参数设定获取设备加速度信息的时间间隔。 加速度信息获取失败则调用回调函数errorCB。

### 参数：
+ successCB: [AccelerometerSuccessCallback](#type_AccelerometerSuccessCallback) `必选` 成功回调函数<br/>
当获取设备的加速度信息成功时回调，并返回加速度信息。
+ errorCB: [AccelerometerErrorCallback](#type_AccelerometerErrorCallback) `可选` 失败回调函数<br/>
当获取设备加速度信息失败回调函数，并返回错误信息。
+ options: [AccelerometerOption](#type_AccelerometerOption) `可选` 加速度信息参数<br/>
监听设备加速度信息的参数，如更新数据的频率等。

### 返回值：
`number`：用于标识加速度信息监听器，可通过 [clearWatch](#api_clearWatch) 方法取消监听。

### 示例：

```
const watchId = watchAcceleration((a) => {
	console.log("Acceleration\nx:" + a.xAxis + "\ny:" + a.yAxis + "\nz:" + a.zAxis);
}, (err) => {
	console.log("watchAcceleration error: " + JSON.stringify(e))
}); // 设置更新间隔时间为1s
```

<a id="api_clearWatch"></a>
## clearWatch
关闭监听设备加速度信息

```
clearWatch (watchId: number): void
```

### 说明：
关闭监听设备加速度信息，应用关闭调用 [watchAcceleration](#api_watchAcceleration) 方法的开启的监听操作。

### 参数：
+ watchId: `number` `必选`<br/>
需要取消的加速度监听器标识，调用 [watchAcceleration](#api_watchAcceleration) 方法的返回值。

### 返回值：
`void`

### 示例：

```
let watchId: number | null = null;
// watchId = watchAcceleration(...);
clearWatch(watchId);
watchId = null;
```

<a id="type_Acceleration"></a>
## Acceleration
设备加速度信息对象

```
export type Acceleration  = {
	/**
	 * x轴方向的加速度
	 * @description 获取当前设备x轴方向的加速度，浮点型数据，与物理学中的加速度值一致。
	 */
	xAxis: number
	/**
	 * y轴方向的加速度
	 * @description 获取当前设备y轴方向的加速度，浮点型数据，与物理学中的加速度值一致。
	 */
	yAxis: number
	/**
	 * z轴方向的加速度
	 * @description 获取当前设备z轴方向的加速度，浮点型数据，与物理学中的加速度值一致。
	 */
	zAxis: number
}
```

### 说明：
保存获取设备的加速度信息，包括x、y、z三个方向的加速度信息。

### 属性：
+ xAxis: x轴方向的加速度</br>
获取当前设备x轴方向的加速度，浮点型数据，与物理学中的加速度值一致。
+ yAxis: y轴方向的加速度</br>
获取当前设备y轴方向的加速度，浮点型数据，与物理学中的加速度值一致。
+ zAxis: z轴方向的加速度<br/>
获取当前设备z轴方向的加速度，浮点型数据，与物理学中的加速度值一致。

<a id="type_AccelerometerOption"></a>
## AccelerometerOption
监听设备加速度感应器参数

```
export type AccelerometerOption = {
	/**
	 * 更新加速度信息间隔时间
	 * @description 监听器获取加速度信息的时间间隔，单位为ms，默认值为500ms
	 */
	frequency: number
}
```

### 说明：
用于设置获取设备加速度信息的参数。

### 属性：
+ frequency: 更新加速度信息间隔时间<br/>
监听器获取加速度信息的时间间隔，单位为ms，默认值为500ms

### 示例：

```
const watchId = watchAcceleration((a) => {
	console.log("Acceleration\nx:" + a.xAxis + "\ny:" + a.yAxis + "\nz:" + a.zAxis);
}, (err) => {
}, { frequency: 1000 } as AccelerometerOption); // 设置更新间隔时间为1s
```

<a id="type_AccelerometerSuccessCallback"></a>
## AccelerometerSuccessCallback
获取设备加速度信息成功的回调函数

```
export type AccelerationSuccessCallback = (acceleration: Acceleration) => void
```

### 参数：
+ acceleration: [Acceleration](#type_Acceleration) 设备的加速度信息<br/>
Acceleration类型对象，用于获取各方向的详细加速度值。

### 返回值：
`void`

<a id="type_AccelerometerErrorCallback"></a>
## AccelerometerErrorCallback
获取设备加速度信息失败的回调函数

```
export type AccelerationErrorCallback = (error: Error) => void
```

### 参数:'
+ error: `Error` 获取加速度操作的错误信息

### 返回值：
`void`

---
### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手